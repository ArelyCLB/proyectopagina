import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
import{ getDatabase,onValue,ref,set,child,get,update,remove }from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";
    import{getAuth, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js";
    const firebaseConfig = {
      apiKey: "AIzaSyBRaafR9lLFpJ2qoJH6GUD9gu73zdRvdys",
      authDomain: "proyecto-final-2a578.firebaseapp.com",
      projectId: "proyecto-final-2a578",
      storageBucket: "proyecto-final-2a578.appspot.com",
      messagingSenderId: "496501604550",
      appId: "1:496501604550:web:6e9fb7abbfbb2b0d3257c7"
    };
  const app = initializeApp(firebaseConfig);
  const db= getDatabase();

  var btnLogin= document.getElementById('btnLogin');
  var btnLimpiar= document.getElementById('btnLimpiar');

  function login(){
    let email = document.getElementById('Usuario').value;
    let contra= document.getElementById('contra').value;

    const auth = getAuth();
    signInWithEmailAndPassword(auth, email, contra)
    .then((userCredential) => {
    // Signed in 
    const user = userCredential.user;
    location.href="/html/admin.html"
    // ...
     })
    .catch((error) => {
    location.href="/html/error.html";
    });
  }

  btnLogin.addEventListener('click', login);